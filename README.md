µ-toksia : SAT-based Solver for Argumentation Frameworks
========================================================

Version    : 2023.08.18

Author     : [Andreas Niskanen](mailto:andreas.niskanen@helsinki.fi), University of Helsinki

Compiling
---------

**NEW:** µ-toksia issues SAT solver calls using the [IPASIR](https://github.com/biotomas/ipasir) interface. Plug in any IPASIR-compatible SAT solver simply by modifying `IPASIRSOLVER` and `IPASIRLIBDIR` in `Makefile`. The recommended SAT solver is [CaDiCaL](https://github.com/arminbiere/cadical) (version 1.9.5), which is included in this release.

Issue `cd lib/cadical-1.9.5 && ./configure -fPIC && make && cd ../..` to build the `libcadical.a` library. Afterwards, issue `make` to build µ-toksia. The binary will be available as `build/release/bin/mu-toksia`.

Command-line usage
------------------

```
build/release/bin/mu-toksia -p <task> -f <file> [-a <query>] [-fo <format>]

  <task>      computational problem
  <file>      input argumentation framework
  <format>    file format for input AF
  <query>     query argument

Options:
  --help      Displays this help message.
  --version   Prints version and author information.
  --formats   Prints available file formats.
  --problems  Prints available computational problems.
```

For a description of possible tasks, see the [ICCMA'23 subtracks](https://iccma2023.github.io/tracks.html#main).

As of ICCMA'23, the input file format flag `-fo` is optional. Without specifying this flag, µ-toksia assumes the [ICCMA'23 format](https://iccma2023.github.io/rules.html#input-format) for the input AF file. (Note that µ-toksia still supports the APX and TGF formats; for a description of these formats, see e.g. Section 4 [here](http://argumentationcompetition.org/2021/SolverRequirements.pdf).)

API usage
---------

µ-toksia supports [IPAFAIR](https://bitbucket.org/coreo-group/ipafair), an incremental API for AF solvers.

**NEW:** By default the SAT solver is never reinitialized and changes to the AF are applied by adding and deactivating clauses. While not recommendeds, to disable this behavior, comment out the line `MUTOKSIA_CXXFLAGS += -D DYNAMIC_MODE` in `Makefile` before compiling.

```
$ python3
>>> from python.mu_toksia import mu_toksia as AFSolver
>>> s = AFSolver("CO")
>>> s.add_argument(2)
>>> s.add_argument(3)
>>> s.add_attack(2,3)
>>> s.solve_cred([3])
False
>>> s.add_argument(1)
>>> s.add_attack(1,2)
>>> s.solve_cred([3])
True
>>> s.extract_witness()
[1, 3]
```

Older versions
--------------

For the ICCMA'19 version (which supports the dynamic track of ICCMA'19), see the [dynamic](https://bitbucket.org/andreasniskanen/mu-toksia/src/dynamic) branch of this repository. For the ICCMA'21 version, please see the commit tagged with [iccma21](https://bitbucket.org/andreasniskanen/mu-toksia/commits/tag/iccma21). For the ICCMA'23 version, please see the commit tagged with [iccma23](https://bitbucket.org/andreasniskanen/mu-toksia/commits/tag/iccma23).

Citation
--------

Please cite the following paper if you use `µ-toksia` in your publication.

```
@inproceedings{DBLP:conf/kr/NiskanenJ20a,
  author    = {Andreas Niskanen and
               Matti J{\"{a}}rvisalo},
  editor    = {Diego Calvanese and
               Esra Erdem and
               Michael Thielscher},
  title     = {{\(\mathrm{\mu}\)}-toksia: An Efficient Abstract Argumentation Reasoner},
  booktitle = {Proceedings of the 17th International Conference on Principles of
               Knowledge Representation and Reasoning, {KR} 2020, Rhodes, Greece,
               September 12-18, 2020},
  pages     = {800--804},
  year      = {2020},
  url       = {https://doi.org/10.24963/kr.2020/82},
  doi       = {10.24963/kr.2020/82},
  timestamp = {Fri, 09 Apr 2021 18:52:15 +0200},
  biburl    = {https://dblp.org/rec/conf/kr/NiskanenJ20a.bib},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```

Contact
-------

Please direct any questions, comments, bug reports etc. directly to [the author](mailto:andreas.niskanen@helsinki.fi).
